// Thomas Keady tkeady1 EN.600.226(01) P2B


import java.util.Iterator;
import java.util.NoSuchElementException;
import java.util.ConcurrentModificationException;
//import static java.lang.IllegalStateException;

/** 
 * Generic sequence interface (positional list).
 *
 * 600.226 Data Structures, Fall 2015, Project 2
 *
 * @param <T> The type of data stored in the linked list
 * @author TKeady
 */
public class SequenceDLL<T> implements Sequence<T> {
    
    /**
     * Inner Node class, declare it first so can immediately
     * get to using it.  
     * @param <T> The type of data to be stored
     */
    private static class Node<T> {
        /**
         * The reference to the next node in the list.
         */
        private Node<T> next;
        
        /**
         * The reference to the previous node in the list.
         */
        private Node<T> prev;
        
        /**
         * The data being stored in the list.
         */
        private T data;
        
        
        /**
         * Constructor leaves links blank so they can be filled in later,
         * but the data is included.  
         * @param dat The actual data to be put there.
         */
        public Node(T dat) {
            this.data = dat;
            this.next = null;
            this.prev = null;
        }
        
    }
    
    /**
     * Reference to first node.
     */
    private Node<T> head;
    
    /**
     * Reference to last node.
     */
    private Node<T> tail;
    
    /**
     * Number of nodes in the list.
     */
    private int size;
    
    /**
     * Mid point of the sequence. Any modification
     * before this starts from the head, after this 
     * starts from the tail.
     */
    private int mid;
    
    /**
     * The "state" of the list, allows iterator to 
     * know if there has beena concurrent modification.
     */
    private long state;
    
    
    /**
     * Constructor for SequenceDLL.
     */
    public SequenceDLL() {
        this.head = null;
        this.tail = null;
        this.size = 0;
        this.state = 0;
        this.mid = 0;
    }
    
    
    /**
     * Insert a new item at a specified position in the sequence.
     * Valid positions are 0 (inclusive) to size() (inclusive).
     * @param item the object to insert
     * @param pos the position in which to insert it. If pos < 0,
     * insert at the beginning of the sequence.  If pos > size(),
     * insert at the end.
     */
    public void addAt(T item, int pos) {
        
        ++this.state;
        
        Node<T> newGuy = new Node<T>(item);
        
        
        if (0 == this.size) {       // Special case for first element
            this.head = newGuy; // Head ptr
            this.tail = newGuy; // Plus tail ptr
            newGuy.next = null; // Both ends null
            newGuy.prev = null;
            
        } else if (pos <= 0) {      // If adding at front
            this.head.prev = newGuy; // The first element's prev is now this
            newGuy.next = this.head; // And the new one's next is it
            this.head = newGuy; // Head points to the new one 
            newGuy.prev = null; // And since its at the front...
            
        } else if (pos >= this.size) {  // If adding at end
            this.tail.next = newGuy; // Attach it to the end
            newGuy.prev = this.tail; // And in reverse
            this.tail = newGuy; // Keep the tail
            newGuy.next = null; // And since its at the end...
            
        } else  if (pos < this.mid) {    // Before the mid point
            
            Node<T> current = this.head;
            
            for (int i = 0; i < pos; ++i) {
                current = current.next; // Walk to where you want it to be
            }
            
            newGuy.next = current;  // Attach all the ends
            newGuy.prev = current.prev;
            current.prev.next = newGuy;
            current.prev = newGuy;
            
        } else {        // After the mid point
            
            Node<T> current = this.tail;
            
            for (int i = 1; i < this.size - pos; ++i) {
                current = current.prev; // Go backwards
            }
            
            newGuy.next = current;  // Attach all the ends
            newGuy.prev = current.prev;
            current.prev.next = newGuy;
            current.prev = newGuy;
            
            
        }
        
        ++this.size;    // Don't forget
        this.mid = this.size / 2;
                
    }


    /** 
     * Return (not remove) the item at a particular position in the sequence.
     * Valid positions are 0 (inclusive) to size() (exclusive).
     * @param pos The position of the item to retrieve
     * @return the item, or null if the position is invalid.
      */
    public T getAt(int pos) {
        
        Node<T> current;
        
        if (pos < 0 || pos >= this.size) {  // Check validity
            return null;
        }
        
        if (pos < this.mid) {    // Before the mid point
            
            current = this.head;
            
            for (int i = 0; i < pos; ++i) {
                current = current.next; // Walk to where you want it to be
            }
            
        } else {        // After the mid point
            
            current = this.tail;
            
            for (int i = 1; i < this.size - pos; ++i) {
                current = current.prev; // Go backwards
            }
            
            
        }
        
        return current.data;    // Get what they want
        
    }


    /** 
     * Remove the item at a particular position in the sequence.
     * Valid positions are 0 (inclusive) to size() (exclusive).
     * @param pos the position of the item to remove
     * @return the item removed, or null if the position is invalid.
     */
    public T removeAt(int pos) {
        
        ++this.state;
        
        Node<T> current = this.head;
        
        if (pos < 0 || pos >= this.size) {
            return null;
        }
        
        int i = 0;
        if (pos < this.mid) {    // Before the mid point
            
            current = this.head;
            
            for (; i < pos; ++i) {
                current = current.next; // Walk to where you want it to be
            }
            
        } else {        // After the mid point
            
            current = this.tail;
            ++i;
            for (; i < this.size - pos; ++i) {
                current = current.prev; // Go backwards
                
            }
            
        }
        
        if (0 == i) {                   // If removing the first one
            current.next.prev = null;
            this.head = current.next;   // Change head
        } else if (current == this.tail) {    // If removing the last one
            current.prev.next = null;
            this.tail = current.prev;   // Change tail
        } else {
            current.next.prev = current.prev;
            current.prev.next = current.next;
        }

        --this.size;    // Don't forget
        this.mid = this.size / 2;
        
        return current.data; // Return the removed data, won't be accessed again
        
    }


    /**
     * Set the value stored at a particular position to a new value.
     * Valid positions are 0 (inclusive) to size() (exclusive).
     * @param val the new value to be stored
     * @param pos the position of the item to be replaced
     * @return the original value at that position, or null if pos is invalid.
     */
    public T setAt(T val, int pos) {
        
        ++this.state;
        
        if (pos >= this.size || pos < 0) {
            return null;    // Bounds checking
        }
        
        
        Node<T> current;
        
        int i = 0;
        
        if (pos < this.mid) {    // Before the mid point
            
            current = this.head;
            
            for (; i < pos; ++i) {
                current = current.next; // Walk to where you want it to be
            }
            
        } else {        // After the mid point
            
            current = this.tail;
            ++i;
            for (; i < this.size - pos; ++i) {
                current = current.prev; // Go backwards
                
            }
            
        }
        
        T old = current.data;   // So we can return the old data
        current.data = val;     // Put in the new stuff
        return old;
        
        
       
    }


    /**
     * Remove all existing elements from the sequence.
     */
    public void clear() {
        this.head = null;   // Just get rid of head and tail
        this.tail = null;   // Garbage collector takes care of the rest
        this.size = 0;
        ++this.state;
    }

    /**
     * Report whether the sequence is empty or not.
     * @return true if the sequence is empty; false otherwise
     */
    public boolean isEmpty() {
        return 0 == this.size;  // NBD
    }

    /**
     * Return the number of items present in the sequence.
     * @return the number of items
     */
    public int size() {
        return this.size;
    }


    /**
     * Determine whether a particular value is in the sequence.
     * @param val The value to look for
     * @return true if there, false otherwise
     */
    public boolean contains(T val) {
        
        Node<T> current = this.head;
        
        for (int i = 0; i < this.size; ++i) {
            if (current.data.equals(val)) {  // If anything ever matches
                return true;            // Return true
            }
            current = current.next;
        }
        
        return false;
    }

    /**
     * Find the position of the first occurrence of a value in the sequence.
     * More efficient to loop again and immediately return i rather than use
     * contains() method and need to loop again if its true.
     * @param val the item to be found
     * @return the position [0,size()), or -1 if not found
     */
    public int position(T val) {
        
        Node<T> current = this.head;
        
        for (int i = 0; i < this.size; ++i) {
            
            if (current.data.equals(val)) {  // If you find it
                return i;               // Return its position
            }
            current = current.next;
        }
        
        return -1;          // Otherwise -1
    }


    /**
     * Return a representation of the items in the sequence,
     * in order from first item to last item.  For example,
     * if the sequence contains three items,  the returned value
     * will be "[item1, item2, item3]"
     * @return the comma-separated items, in square brackets
     */
    public String toString() {
        String seq = "[";   // Start of string
        
        Node<T> current = this.head;
        
        if (this.head == null) {
            return "[]";
        } else {    // Case for first node
            seq = seq + this.head.data.toString();
        }
        
        while (current.next != null) {
            if (current.next != null) { // If theres another one, need ", "
                seq += ", ";
            }
            current = current.next;     // Add the next one
            seq += current.data.toString();
            
        }
        
        seq += "]"; // End of string
        return seq;
        
    }

    /**
     * Customized iterator for this class.
     * @author TKeady
     *
     */
    private class SequenceDLLIterator implements Iterator<T> {
        
        /**
         * The position the iterator starts at.
         */
        Node<T> current = SequenceDLL.this.head;
        
        
        /**
         * The number of steps the iterator has taken through the list.
         */
        int steps = 0;
        
        /**
         * The position of the iterator in the list.
         * Is affected by remove() calls.
         */
        int position = 0;
        
        /**
         * Checks whether the iterator has called next() since the last
         * call to remove(), to make sure everything is hit once.
         */
        boolean okToRemove = false;
        
        /**
         * The state of the list when the iterator is created.
         */
        long startState = SequenceDLL.this.state;
        
        /**
         * Returns true if there is another item in the sequence.
         * That item (if it exists) will be returned on the next
         * call of next()
         * @return boolean True if there is another item, false otherwise
         * @throws ConcurrentModificationException If the list has been 
         *         modified outside of the iterator
         */
        @Override
        public boolean hasNext() {
            if (SequenceDLL.this.state != this.startState) {
                throw new ConcurrentModificationException();
            }
            
            return this.current != null;
        }
        
        /**
         * Gets the next item in the sequence.  
         * @return T The item, will only ever return this.
         * @throws NoSuchElementException If there is no next
         *         element, in which case it does not return
         * @throws ConcurrentModificationException Like hasNext()  
         */
        @Override
        public T next() {
            if (SequenceDLL.this.state != this.startState) {
                throw new ConcurrentModificationException();
            }
            
            if (this.hasNext()) {   // If we good
                T toReturn = this.current.data; // Get what you want to return 
                this.current = this.current.next;   // Step for next time
                ++this.steps;           // Make sure everything makes sense
                ++this.position;        
                this.okToRemove = true; // This can be removed if you want
                
                return toReturn;
                
            } else {
                throw new NoSuchElementException();
            }
        }
        
        /**
         * Removes an element from the sequence in a safe way,
         * aka the iterator knows it has been covered and is
         * now removed, without disrupting the rest of the 
         * elements iterator must return (or any other elements
         * for that matter).  
         * @throws ConcurrentModificationException If something else
         *         modified it
         * @throws IllegaLStateException If you try to remove something
         *         when its not OK to 
         */
        @Override
        public void remove() {
            if (SequenceDLL.this.state != this.startState) {
                throw new ConcurrentModificationException();
            }
            
            if (this.okToRemove) {
                SequenceDLL.this.removeAt(this.position - 1);
                --SequenceDLL.this.state; // Since remove increments it but 
                                          // removal here is allowed
            } else {
                throw new IllegalStateException();
            }
            --this.position;        // So the iterator knows its been removed
            this.okToRemove = false;    // Don't try to remove again
        }
        
        
        
    }

    /**
     * Iterator constructor.
     */
    @Override
    public Iterator<T> iterator() {
        return new SequenceDLLIterator();
    }

    
    
}
