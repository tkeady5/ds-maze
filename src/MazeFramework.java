// Thomas Keady tkeady1 600.226(01) P2A

/**
 * MazeFramework is the actual instance of a maze that will be displayed with
 * MazeRenderer.   
 * @author TKeady
 *
 */
public class MazeFramework implements MazeSolver {

    /**
     * The primary structure inside MazeFramework class.  It is an array 
     * of array of Cells that when depicted visually is the actual maze.  
     * MazeRenderer decides what to show based on the truth values of
     * walls inside the cells.  
     */
    private Cell[][] maze;

    /**
     * BagArray of Walls, each wall is single sided.
     */
    private BaGArray<Wall> walls;

    /**
     * UnionFind parent pointer tree of "cells".
     * Each cell is actually represented by its an 
     * integer, specifically the array index 
     */
    private UnionFind paths;


    /**
     * Constructor for the Maze object.  
     * @param rows The number of rows in the maze
     * @param cols The number of columns in the maze
     */
    public MazeFramework(int rows, int cols) {

        this.maze = new Cell[rows][cols];

        this.walls = new BaGArray<Wall>();

        this.paths = new UnionFind(rows * cols);

        // The number location of the cell
        int cellNum = 0;

        for (int r = 0; r < rows; ++r) {
            for (int c = 0; c < cols; ++c) {
                Cell cell = new Cell();
                this.maze[r][c] = cell;

                // If it is on the top row, dont add north
                if (r != 0) {
                    this.walls.add(new Wall(cellNum, "north"));
                }

                // If it is on the bottom row, dont add south
                if (r != rows - 1) {
                    this.walls.add(new Wall(cellNum, "south"));
                }

                // If it is in the left column
                if (c != 0) {
                    this.walls.add(new Wall(cellNum, "west"));
                }

                // If it is in the right column
                if (c != cols - 1) {
                    this.walls.add(new Wall(cellNum, "east"));
                }

                ++cellNum;
            }
        }

        /*System.out.println(this.walls.toString() + "\nnumRows: " + rows 
                + "\nnumCols: " + cols);*/

    }



    /**
     * Generate the maze.  This takes a constructed Maze object with
     * all of its walls present, and randomly removes interior walls until all 
     * cells of the maze are reachable from all other cells of the maze. 
     * Also removes the west wall of top left corner cell (for a maze start)
     * and the east wall of bottom right corner cell (for a maze finish line).
     */
    public void generateMaze() {

        // First remove the top left and bottom right walls
        this.maze[0][0].setWest(false);
        this.maze[this.getNumRows() - 1][this.getNumCols() - 1].setEast(false);


        /*while (!this.paths.haveSameRoot(0, 
                this.getNumRows() * this.getNumCols() - 1)) {*/
        while (!this.walls.isEmpty()) {

            Wall removed = this.walls.remove();
            int cellNum = removed.getCell();
            String side = removed.getSide();

            // This coordinate is equal to the remainder of 
            // cellNum / columns (length of rows)  
            int rowi = cellNum / this.getNumCols();

            // For column location take modulus 
            //int coli = cellNum % this.getNumRows();
            int coli = cellNum % this.getNumCols();

            /*System.out.println("cellNum: " + cellNum + "\trowi: " + rowi 
                    + "\tcoli: " + coli + "\tside: " + side);*/
            
            switch (side) {    
                case "north":  
                    
                    if (!this.paths.haveSameRoot(cellNum, cellNum
                            - this.getNumCols())) {
                        this.maze[rowi][coli].setNorth(false);
                        this.maze[rowi - 1][coli].setSouth(false);
                        
                        this.paths.union(cellNum, cellNum - this.getNumCols());
                    }
                    
                    break;
                case "east":
                    
                    if (!this.paths.haveSameRoot(cellNum, cellNum + 1)) {
                        this.maze[rowi][coli].setEast(false);
                        this.maze[rowi][coli + 1].setWest(false);

                        this.paths.union(cellNum, cellNum + 1);
                    }
                    
                    break;
                case "south":
                    
                    if (!this.paths.haveSameRoot(cellNum, 
                            cellNum + this.getNumCols())) {
                        this.maze[rowi][coli].setSouth(false);
                        this.maze[rowi + 1][coli].setNorth(false);
                        
                        this.paths.union(cellNum, cellNum + this.getNumCols());
                    }
                    
                    
                    break;
                case "west":
                    
                    if (!this.paths.haveSameRoot(cellNum, cellNum - 1)) {
                        this.maze[rowi][coli].setWest(false);
                        this.maze[rowi][coli - 1].setEast(false);

                        this.paths.union(cellNum, cellNum - 1);
                    }
                    
                    
                    break;
                default:
                    System.out.println("You done goofed");
                    break;
    

            }
        
        }
    
    }

    /**
     * Return the Cell object stored at the given (row, column) position.
     * @param r the row position of the Cell in the Maze object
     * @param c the col position of the Cell in the Maze object
     * @return the Cell object that is at the specified position
     */
    public Cell getCellAt(int r, int c) {
        return this.maze[r][c];
    }


    /**
     * Set the cell at the given (row, column) position to the provided cell.
     * @param r the row position of the new Cell in the maze
     * @param c the column position of the new Cell in the maze
     * @param cell the new Cell object to be set in the specified position
     */
    public void setCellAt(int r, int c, Cell cell) {
        this.maze[r][c] = cell;
    }


    /**
     * Return the number of rows in the maze.
     * @return the number of rows in the maze
     */
    public int getNumRows() {
        return this.maze.length;    // not sure if this is right
    }

    /**
     * Return the number of columns in the maze.
     * @return the number of columns in the maze
     */
    public int getNumCols() {
        return this.maze[0].length;   // not sure of this either
    }



    
    
    /** 
     * Read a string containing bit representations of each cell in
     * the maze, in standard row-column order, separated by
     * whitespace.  Use this to initialize the state of each cell.
     * @param s the string of all cell data
     */
    public void readMaze(String s) { 
        
        int stringi = 0;    // used for charAt
        
        int rows = this.maze.length;
        int cols = this.maze[0].length;
        
        for (int r = 0; r < rows; ++r) {
            for (int c = 0; c < cols; ++c) {
                // Go to helper function
                stringi = this.cellSetter(stringi, s, r, c);
                
                if (stringi < s.length()) {
                    // In case it has spaces before the newlines
                    if (Character.isWhitespace(s.charAt(stringi))) {      
                        ++stringi;  
                    } else if (Character.isWhitespace(s.charAt(stringi))) {
                        stringi += 2;
                    } 
                
                } 
                
                
            }
        }
        
    }
    
    /**
     * Helper function to reduce cyclomatic complexity of readMaze().
     * @param stringi The position in the string
     * @param s The string itself
     * @param r The row of the cell
     * @param c The col of the cell
     * @return stringi
     */
    private int cellSetter(int stringi, String s, int r, int c) {
        if (0 == (int) s.charAt(stringi++) - (int) '0') {     
            this.maze[r][c].setNorth(false); // Again this is kind of
        }                                    // ugly but idk how else
        
        if (0 == (int) s.charAt(stringi++) - (int) '0') {
            this.maze[r][c].setWest(false);
        } 
        
        if (0 == (int) s.charAt(stringi++) - (int) '0') {
            this.maze[r][c].setSouth(false);
        } 

        if (0 == (int) s.charAt(stringi++) - (int) '0') {
            this.maze[r][c].setEast(false);
        } 
        
        if (1 == (int) s.charAt(stringi++) - (int) '0') {
            this.maze[r][c].setVisited();
        }
        
        if (1 == (int) s.charAt(stringi++) - (int) '0') {
            this.maze[r][c].setPath();
        }
        
        return stringi;
    }

    /**
     * Clear the visited and path fields of all the cells in the maze,
     * resetting them to false.  Order of this does not matter so goes
     * through list backwards to save need for another variable.
     */
    public void clearSearch() {        
        
        for (int i = this.maze.length - 1; i >= 0; --i) {
            for (int j = this.maze[0].length - 1; j >= 0; --j) { 
                this.maze[i][j].unsetVisited();
                this.maze[i][j].unSetPath();
            }
        }
    }


    /**
     * Perform a depth-first-search on the maze to find a path from
     * the start (top-left cell) to the finish (bottom-right cell),
     * using a Stack (recursion is forbidden).
     * @param s the stack to use in finding a solution
     */
    public void solveDFS(Stack<Cell> s) {
        this.maze[0][0].setCameFrom("west");
        this.maze[0][0].setVisited();
        this.maze[0][0].setPath();
                
        s.push(this.maze[0][0]);
        
        
     // Represent the position of the cell at the top and the previous one
        int i = 0, j = 0;
        
        while (s.top()  // While its not equal to the last cell
                != this.maze[this.maze.length - 1][this.maze[0].length - 1]) {
            
            //System.out.println(s.top().toString());
            if (!s.top().hasEast() // If no east wall and havnt visited that one
                    && !this.maze[i][j + 1].wasVisited()) {
                
                this.maze[i][++j].setVisited(); // Autoincrement j for position
                this.maze[i][j].setPath();
                this.maze[i][j].setCameFrom("west");
                
                s.push(this.maze[i][j]);    // Put the new one on the stack
                //System.out.println("east is beast");
            } else if (!s.top().hasSouth() 
                    && !this.maze[i + 1][j].wasVisited()) {


                this.maze[++i][j].setVisited();
                this.maze[i][j].setPath();
                this.maze[i][j].setCameFrom("north");

                s.push(this.maze[i][j]);
                //System.out.println("south side");
            } else if (!s.top().hasWest()
                    && !this.maze[i][j - 1].wasVisited()) {

                this.maze[i][--j].setVisited();
                this.maze[i][j].setPath();
                this.maze[i][j].setCameFrom("east");
                
                s.push(this.maze[i][j]);
                //System.out.println("in west philadelphia");
            } else if (!s.top().hasNorth()
                    && !this.maze[i - 1][j].wasVisited()) {

                this.maze[--i][j].setVisited();
                this.maze[i][j].setPath();
                this.maze[i][j].setCameFrom("south");
                
                s.push(this.maze[i][j]);
                //System.out.println("yankee");
            } else {    // If there are no accessible adjacent cells
                // that have not been visited yet, pop it off
                this.maze[i][j].unSetPath();
                /*System.out.println(i + "\t" + j);
                System.out.println(this.maze[i][j].toString());
                System.out.println(s.size());*/
                int toi = this.changei(this.maze[i][j]);
                j += this.changej(this.maze[i][j]);  // Change i and j
                i += toi;                           // to get to prev cell
                
                s.pop();
                

            }
            
            //System.out.println("End: " + i + "\t" + j);
            
        }
        // Now its equal to the last cell so we found the solution
        
        
        
        
        
    }
    
    
    /**
     * Helper method to reduce DFS's cyclomatic complexity.
     * Returns -1, 0, or 1 depending on how i must be changed.
     * @param t The cell at the top of the stack.
     * @return i The number to add to i. 
     */
    private int changei(Cell t) {
        if (t.getCameFrom().equals("north")) {
            return -1;
        } else if (t.getCameFrom().equals("south")) {
            return 1;
        } else {
            return 0;
        }
    }
    
    
    /**
     * Helper method to reduce DFS's cyclomatic complexity.
     * Returns -1, 0, or 1 depending on how j must be changed.
     * @param t The cell at the top of the stack.
     * @return j The number to add to j. 
     */
    private int changej(Cell t) {
        if (t.getCameFrom().equals("west")) {
            return -1;
        } else if (t.getCameFrom().equals("east")) {
            return 1;
        } else {
            return 0;
        }
    }
    

    /**
     * Perform a breadth-first-search on the maze to find a path from
     * the start (top-left cell) to the finish (bottom-right cell),
     * using a Queue (recursion is forbidden).
     * @param q the queue to use in finding a solution
     */
    public void solveBFS(Queue<int[]> q) {
        
        int[] pos = {0, 0};
        //int[] newOne = {0, 0};
        System.out.println("\t\t" + q.toString());
        q.clear();
        q.enqueue(pos);
        System.out.println("\t\t" + q.toString());
        System.out.println("\n\n");
        
        System.out.println("\t end: " 
                + this.maze[this.maze.length - 1]
                [this.maze[0].length - 1]);
        
        
        boolean pathFound = false;
        
        while (!pathFound) {
            
            pos = q.peek();
            
            System.out.println("size " + q.size() 
                + " pos " + pos[0] + " " + pos[1]);
            
            

            
            if (pos[0] == this.maze.length - 1 
                    && pos[1] == this.maze[0].length - 1) {
                    
                pathFound = true;
            }
            
            this.checkEast(q, pos);
            
            this.checkSouth(q, pos);
            
            this.checkWest(q, pos);
            
            this.checkNorth(q, pos);
            
            
            
            System.out.println("\t removing " + pos[0] + " " + pos[1]);
            this.maze[pos[0]][pos[1]].setVisited();
            q.dequeue();
            
            System.out.println("\t\t" + q.toString());
            
        } // end while
        
        Cell goingBack = this.maze[this.maze.length - 1]
                [this.maze[0].length - 1];
        
        goingBack.setPath();
        this.pathSetter(q, goingBack);
        
        
        
        
    }
    
    
    /**
     * Helper function to decrement cyclomatic complexity.
     * @param q The queue
     * @param pos Position array.
     */
    private void checkNorth(Queue<int[]> q, int[] pos) {
        
        if (pos[0] == 0) {  // Prevents it from searching north in case
            return;         // a wall is missing there
        }
        
        if (!this.maze[pos[0]][pos[1]].hasNorth()
                && !this.maze[pos[0] - 1][pos[1]].wasVisited()) {
            
            int[] newOne = new int[2];
            newOne[0] = pos[0] - 1;     // Make an array representing the
            newOne[1] = pos[1]; // cell to the north
                                    
            
            this.maze[pos[0] - 1][pos[1]].setAddedBy(
                    this.maze[pos[0]][pos[1]]);
            System.out.println("\t adding " + newOne[0] + " " + newOne[1]);
            q.enqueue(newOne);
        }
    }
    
    /**
     * Helper function to decrement cyclomatic complexity.
     * @param q The queue
     * @param pos Position array.
     */
    private void checkSouth(Queue<int[]> q, int[] pos) {
        
        if (pos[0] == this.maze.length - 1) {  // Prevents it from searching 
            return;         // north in case a wall is missing there
        }
        
        if (!this.maze[pos[0]][pos[1]].hasSouth()
                && !this.maze[pos[0] + 1][pos[1]].wasVisited()) {
            
            int[] newOne = new int[2];
            newOne[0] = pos[0] + 1;     // Make an array representing the
            newOne[1] = pos[1]; // cell to the south
                                    
            
            this.maze[pos[0] + 1][pos[1]].setAddedBy(
                    this.maze[pos[0]][pos[1]]);
            System.out.println("\t adding " + newOne[0] + " " + newOne[1]);
            q.enqueue(newOne);
            
        }
    }

    
    
    
    /**
     * Helper function to decrement cyclomatic complexity.
     * @param q The queue
     * @param pos Position array.
     */
    private void checkWest(Queue<int[]> q, int[] pos) {
        
        if (pos[1] == 0) {  // Prevents it from searching north in case
            return;         // a wall is missing there
        }
        
        if (!this.maze[pos[0]][pos[1]].hasWest()
                && !this.maze[pos[0]][pos[1] - 1].wasVisited()) {

            int[] newOne = new int[2];
            newOne[0] = pos[0];     // Make an array representing the
            newOne[1] = pos[1] - 1; // cell to the west


            this.maze[pos[0]][pos[1] - 1].setAddedBy(
                    this.maze[pos[0]][pos[1]]);
            System.out.println("\t adding " + newOne[0] + " " + newOne[1]);
            q.enqueue(newOne);

        }
    }
    
    
    /**
     * Helper function to decrement cyclomatic complexity.
     * @param q The queue
     * @param pos Position array.
     */
    private void checkEast(Queue<int[]> q, int[] pos) {
        
        if (pos[1] == this.maze[0].length - 1) {  // Prevents it from searching 
            return;         // north in case a wall is missing there
        }
        
        // If it has no wall and that cell has not been visited
        if (!this.maze[pos[0]][pos[1]].hasEast()
                && !this.maze[pos[0]][pos[1] + 1].wasVisited()) {
            
            int[] newOne = new int[2];
            newOne[0] = pos[0];     // Make an array representing the
            newOne[1] = pos[1] + 1; // cell to the east
                                    
            // So we know where it came from
            this.maze[pos[0]][pos[1] + 1].setAddedBy(
                    this.maze[pos[0]][pos[1]]);
            System.out.println("\t adding " + newOne[0] + " " + newOne[1]);
            q.enqueue(newOne); // Put it in the queue
            // Note it is not visited yet, that happens once we reach it 
            // in the queue
            
        }
    }
    
    
    
    
    
    /** 
     * Helper function for BDS that reduces cyclomatic complexity.
     * Steps back via the Cell's addedBy member to mark all the 
     * cells in the path.  
     * @param q The queue.
     * @param c The last cell in the solution. 
     */
    private void pathSetter(Queue<int[]> q, Cell c) {
        while (c != this.maze[0][0]) {
            
            c = c.whoAdded();
            c.setPath();
            
        }
    }
    
    
    

    /**
     * This method must create and return one (long) string that
     * contains the row and column dimensions of the maze, then a
     * newline, followed by the string representation of each cell,
     * one row at a time, with each cell separated from the next with
     * a space and each row separated from the next by a newline
     * ('\n').
     * @return the string representation
     */
    public String toString() {
        int endi = this.maze.length;
        int endj = this.maze[0].length;
        String toReturn = endi + " " + endj + "\n";
        for (int i = 0; i < endi; ++i) {
            for (int j = 0; j < endj; ++j) {
                
                if (this.maze[i][j].hasNorth()) {   // I couldn't think
                    toReturn += 1;                  // of a cleaner way
                } else {                            // :'(
                    toReturn += 0;
                }

                if (this.maze[i][j].hasWest()) {
                    toReturn += 1;
                } else {
                    toReturn += 0;
                }

                if (this.maze[i][j].hasSouth()) {
                    toReturn += 1;
                } else {
                    toReturn += 0;
                }

                if (this.maze[i][j].hasEast()) {
                    toReturn += 1;
                } else {
                    toReturn += 0;
                }

                if (this.maze[i][j].wasVisited()) {
                    toReturn += 1;
                } else {
                    toReturn += 0;
                }

                if (this.maze[i][j].isPath()) {
                    toReturn += 1;
                } else {
                    toReturn += 0;
                }
                  
                toReturn += " ";
                
            }
            
            toReturn += "\n";
            
        }
        
        return toReturn;
    }



}
