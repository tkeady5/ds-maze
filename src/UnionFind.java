// Thomas Keady tkeady1 600.226(01) P2A

/**
 * Class implements union/find methods on a parent pointer
 * tree.  The tree will be a tree of cells, and checking whether
 * cells are in the same tree will tell us whether a path in the 
 * maze connects them or not.  
 * 
 * Part of this code has been copied and modified from 
 * http://algoviz.org/OpenDSA/Books/CS226JHUF15/html/UnionFind.html
 * 
 * @author OpenDSA Project Contributors
 * @author TKeady (modified code from above authors)
 *
 */
public class UnionFind {

    // Copied code starts here (modifications have been made)
    
    /**
     * The standard array that holds the nodes. 
     */
    private int[] array;     // Node array
    
    /**
     * The standard array that holds the size of the list headed by 
     * the node at the respective position in array.
     */
    private int[] weights;

    /**
     * Constructor, makes array of singleton sets.
     * @param size The size of the set.
     */
    public UnionFind(int size) {
        this.array = new int[size];     // Create node array
        this.weights = new int[size];   // Create weights array
        for (int i = 0; i < size; ++i) {
            this.array[i] = -1;         // Each node is its own root to start
            this.weights[i] = 1;        // Each tree starts as size 1
            
        }
    }

    /**
     * Combines two sets if they are different (do not have
     * the same parent root.
     * @param a The first set
     * @param b The second set.
     */
    void union(int a, int b) {
        int root1 = this.find(a);       // Find root of node a
        int root2 = this.find(b);       // Find root of node b
        if (root1 != root2) {           // Merge with weighted union
            if (this.weights[root2] > this.weights[root1]) {
                this.array[root1] = root2;
                this.weights[root2] += this.weights[root1];
            } else {
                this.array[root2] = root1;
                this.weights[root1] += this.weights[root2];
            }
        }
    }

    
    /**
     * Recursively goes up the tree to find the given node's
     * root.  Simultaneously implements path compression to 
     * increase speed.  
     * @param curr Any node
     * @return The root of the tree
     */
    int find(int curr) {
        if (this.array[curr] == -1) {
            return curr; // At root
        }
        this.array[curr] = this.find(this.array[curr]);
        return this.array[curr];
    }
    
    // Copied code ends here 
    
    /**
     * Checks whether two nodes have the same parent.
     * @param a One node
     * @param b The other
     * @return True if the nodes are in the same tree, false otherwise
     */
    public boolean haveSameRoot(int a, int b) {
        int root1 = this.find(a);       // Find root of node a
        int root2 = this.find(b);       // Find root of node b
        return root1 == root2;
    
    }
    
    
}
