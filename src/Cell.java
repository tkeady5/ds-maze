/**
 * Cell represents a single tile of a maze.
 *
 * 600.226 Data Structures, Fall 2015, Project 2
 *
 */
public class Cell {

    /** whether or not wall on north side exists. */
    private boolean north; 
    /** whether or not wall on south side exists. */
    private boolean south; 
    /** whether or not wall on east side exists. */
    private boolean east; 
    /** whether or not wall on west side exists. */
    private boolean west; 
    
    /** Whether this cell has been visited by the solver. */
    private boolean visited;
    /** Whether this cell is on the solution path. */
    private boolean path;
    /** What cell added this one to a stack/queue. */
    private Cell addedBy;
    /** Direction it was added from. */
    private String cameFrom;
    
    /**
     * Cell constructor, has four walls by default.
     */
    public Cell() {
        this.north = true;
        this.south = true;
        this.west = true;
        this.east = true;
        
        this.visited = false;
        this.path = false;
    }
    
    /**
     * Cell constructor with defined wall parameters.
     * @param n true if north end of the tile has a wall
     * @param s true if south end of the tile has a wall
     * @param w true if west end of the tile has a wall
     * @param e true if east end of the tile has a wall
     */
    public Cell(boolean n, boolean s, boolean e, boolean w) {
        this.north = n;
        this.south = s;
        this.east = e;
        this.west = w;
        
        this.visited = false;
        this.path = false;
    }
    
    
    // Getters and Setters
    
    /**
     * Return whether this cell's north wall exists.
     * @return true if and only if the north wall exists
     */
    public boolean hasNorth() {
        return this.north;
    }

    /**
     * Indicate whether this cell's north wall should exist.
     * @param northVal  true if wall exists; false otherwise
     */
    public void setNorth(boolean northVal) {
        this.north = northVal;
    }

    /**
     * Return whether this cell's south wall exists.
     * @return true if and only if the south wall exists
     */
    public boolean hasSouth() {
        return this.south;
    }

    /**
     * Indicate whether this cell's south wall should exist.
     * @param southVal  true if wall exists; false otherwise
     */
    public void setSouth(boolean southVal) {
        this.south = southVal;
    }

    /**
     * Return whether this cell's west wall exists.
     * @return true if and only if the west wall exists
     */
    public boolean hasWest() {
        return this.west;
    }

    /**
     * Indicate whether this cell's west wall should exist.
     * @param westVal true if wall exists; false otherwise
     */
    public void setWest(boolean westVal) {
        this.west = westVal;
    }

    /**
     * Return whether this cell's east wall exists.
     * @return true if and only if the east wall exists
     */
    public boolean hasEast() {
        return this.east;
    }

    /**
     * Indicate whether this cell's east wall should exist.
     * @param eastVal  true if wall exists; false otherwise
     */
    public void setEast(boolean eastVal) {
        this.east = eastVal;
    }
    
    /**
     * Set when the cell has been visited.
     */
    public void setVisited() {
        this.visited = true;
    }
    
    /**
     * Tells you whether it has been visited.  
     * @return boolean The cell's visited variable
     */
    public boolean wasVisited() {
        return this.visited;
    }
    
    /**
     * Exists because of the clear() function.
     */
    public void unsetVisited() {
        this.visited = false;
    }
    
    /**
     * Sets when the cell is found to be part of the solution.  
     */
    public void setPath() {
        this.path = true;
    }
    
    /**
     * Unsets whether it is part of the solution (may not be necessary).
     */
    public void unSetPath() {
        this.path = false;
    }
    
    /**
     * Tells you whether it is part of the solution.
     * @return boolean The cell's path variable.
     */
    public boolean isPath() {
        return this.path;
    }
    
    /**
     * Sets which cell this one was added by.  No need to
     * unset it because cells should only be visited once.  And
     * if they are visited more than once it would be via the same cell anyway.
     * @param c The cell it was added by.
     */
    public void setAddedBy(Cell c) {
        this.addedBy = c;
    }
    
    /**
     * Tells the world who added it.
     * @return The cell it was added by.  
     */
    public Cell whoAdded() {
        return this.addedBy;
    }
    
    /**
     * Sets the direction the cell was added from.  Can only be
     * "north", "west", "south" or "east".
     * @param s The string holding the direction
     */
    public void setCameFrom(String s) {
        this.cameFrom = s;
    }
    
    /**
     * Tells the direction it came from.
     * @return The direction this cell was added from
     */
    public String getCameFrom() {
        return this.cameFrom;
    }
    
    
    /**
     * For debugging.  
     * @return The string
     */
    @Override
    public String toString() {
        String toReturn = "";
        
        if (this.hasNorth()) { 
            toReturn += 1;
        } else {
            toReturn += 0;
        }
        
        if (this.hasWest()) {
            toReturn += 1;
        } else {
            toReturn += 0;
        }
        
        if (this.hasSouth()) {
            toReturn += 1;
        } else {
            toReturn += 0;
        }
        
        if (this.hasEast()) {
            toReturn += 1;
        } else {
            toReturn += 0;
        }
        
        if (this.wasVisited()) {
            toReturn += 1;
        } else {
            toReturn += 0;
        }
        
        if (this.isPath()) {
            toReturn += 1;
        } else {
            toReturn += 0;
        }
        
        return toReturn;
    }
     
     
    
}
