import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertSame;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.fail;

import org.junit.BeforeClass;
import org.junit.Before;
import org.junit.Test;

public class QueueSeqTest {

    static Queue<Integer> qint;
    static Queue<String>  qstr;
    static final int[] values = {23, 24, 25, 1, 2, 3};
    static final String valString = "FRONT __ 23 __ 24 __ 25 __ 1 __ 2 __ 3 __ BACK";
    static final String emptyQ = "FRONT __ BACK";

    @BeforeClass
    public static void init() {
        qint = new QueueSeq<Integer>();
        qstr = new QueueSeq<String>();
    }

    @Before
    public void setup() { // dependent on clear being good
        qint.clear();
        qstr.clear();
    }

    @Test
    public void testClear() {
        Queue<Integer> qfl = new QueueSeq<Integer>();
        assertEquals(qfl.size(), 0);
        assertTrue(qfl.isEmpty());
        // pretend that enqueue works
        for (int i=0; i < values.length; i++) {
            qfl.enqueue(values[i]);
        }
        assertEquals(qfl.toString(), valString);
        qfl.clear();
        assertTrue(qfl.isEmpty());
        assertEquals(qfl.size(), 0);
        assertEquals(qfl.toString(), emptyQ);
    }

    @Test
    public void testSize() {
        assertEquals(qstr.size(), 0);
        qstr.enqueue("one");
        assertEquals(qstr.size(), 1);
        qstr.enqueue("two");
        assertEquals(qstr.size(), 2);
        for (int i=0; i < values.length; i++) {
            qint.enqueue(values[i]);
        }
        assertEquals(qint.size(), values.length);
        qint.clear();
        assertEquals(qint.size(), 0);
    }

    @Test
    public void testIsEmpty() {
        assertTrue(qstr.isEmpty());
        for (int i=0; i < values.length; i++) {
            qint.enqueue(values[i]);
        }
        assertFalse(qint.isEmpty());
        qstr.enqueue("one");
        assertFalse(qstr.isEmpty());
        qstr.peek();
        assertFalse(qstr.isEmpty());
        qstr.dequeue();
        assertTrue(qstr.isEmpty());
    }

    @Test
    public void testEnqueue() {  // also tests toString()
        assertEquals(qstr.size(), 0);
        assertEquals(qstr.toString(), emptyQ);
        qstr.enqueue("one");
        assertEquals(qstr.toString(), "FRONT __ one __ BACK");
        qstr.enqueue("two");
        assertEquals(qstr.toString(), "FRONT __ one __ two __ BACK");
        qstr.enqueue("tre");
        assertEquals(qstr.toString(), "FRONT __ one __ two __ tre __ BACK");
        for (int i=0; i < values.length; i++) {
            qint.enqueue(values[i]);
        }
        assertEquals(qint.toString(), valString);
    }

    @Test
    public void testPeek() {
        assertNull(qstr.peek());  // empty
        qstr.enqueue("one");
        assertEquals(qstr.peek(), "one");
        qstr.enqueue("two");
        assertEquals(qstr.peek(), "one");
        assertEquals(qstr.size(), 2);
        qstr.enqueue("tre");
        assertEquals(qstr.peek(), "one");
        qstr.clear();
        assertNull(qstr.peek());  // empty
    }

    @Test
    public void testDequeue() {
        assertNull(qstr.dequeue());  // empty
        qstr.enqueue("one");
        assertEquals(qstr.dequeue(), "one");
        assertTrue(qstr.isEmpty());
        qstr.enqueue("one");
        qstr.enqueue("two");
        assertEquals(qstr.dequeue(), "one");
        assertEquals(qstr.size(), 1);
        qstr.enqueue("tre");
        assertEquals(qstr.dequeue(), "two");
        assertEquals(qstr.size(), 1);
        qstr.clear();
        assertNull(qstr.dequeue());  // empty
    }

}