import java.io.FileWriter;
import java.io.IOException;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.Scanner;  // for Scanner and some exceptions
import java.util.InputMismatchException;

/**
 * Driver program to run the MazeSolver algorithms.
 *
 * 600.226 Data Structures, Fall 2015, Project 2
 *
 * CAUTION - this file is not yet checked for compilation or style errors!
 *
 */

public final class MazeSolveDriver {

    /** 
     * Useless constructor.
     */
    private MazeSolveDriver() {

    }

    /**
     * Main driver program.
     * @param args the command-line argument - should be a plain text filename
     * @throws IOException if files cannot be opened
     */
    public static void main(String[] args) throws IOException {

        Scanner fscan = null;
        String fname = "";  // without the ".txt"

        try {
            // get command-line argument which is the name of a 
            // maze text file such as filename.txt
            fscan = new Scanner(new FileReader(args[0]));

            // strip the .txt from the name
            int where = args[0].indexOf(".txt");
            fname = args[0].substring(0, where);
        } catch (IOException ioe) {
            System.out.println("ERROR: can't read file " + args[0]);
            throw ioe;  // can't recover from this
        } catch (ArrayIndexOutOfBoundsException aie) {
            System.err.println("ERROR: command-line text filename expected");
            System.exit(1);
        } catch (StringIndexOutOfBoundsException aie) {
            System.err.println("ERROR: filename must end in .txt");
            System.exit(2);
        }
        

        // read the dimensions of the maze from the file
        int rows, cols;
        try {
            rows = fscan.nextInt();
            cols = fscan.nextInt();
            if (rows < 1 || cols < 1) {
                throw new IllegalArgumentException("Both arguments must be "
                    + "positive.");
            }
        } catch (InputMismatchException ime) {
            System.err.println("ERROR: file must start with int rows & cols");
            throw ime;
        }

        // create solvable maze (but do not generate)
        MazeSolver mazy = new MazeFramework(rows, cols);

        // read cell data from file into one big-ass string
        String big = "";
        int count = 0;
        while (fscan.hasNext()) {
            big += fscan.next() + " ";
            count++;
        }
        big = big.substring(0, big.length() - 1); // strip last space
        if (count != rows * cols) {
            System.err.println("ERROR: tokens must match number of cells");
            throw new IOException();
        } 

        // call readMaze function with big-ass string
        mazy.readMaze(big);
        
        // clear the visited and path fields for all cells (to be safe)
        mazy.clearSearch();

        // make an empty stack
        Stack<Cell> stk = new StackSeq<Cell>();
        // do depth-first solution search using the stack
        mazy.solveDFS(stk);
        // write solved maze to filenameDFS.txt
        PrintWriter fileDFS = new PrintWriter(new FileWriter(fname
            + "DFS.txt"));
        fileDFS.println(mazy);
        fileDFS.close();
        // clear the visited and path fields for all cells
        mazy.clearSearch();

        // make an empty queue
        Queue<int[]> que = new QueueSeq<int[]>();
        // do breadth-first solution search using the queue
        mazy.solveBFS(que);
        // write solved maze to filenameBFS.txt
        PrintWriter fileBFS = new PrintWriter(new FileWriter(fname
            + "BFS.txt"));
        fileBFS.println(mazy);
        fileBFS.close();
    }

}