/** 
 * Interface with methods to read, clear and solve a maze.
 * @author cs226, Project 2, Fall 2015
 */
public interface MazeSolver extends Maze {

    /** 
     * Read a string containing bit representations of each cell in
     * the maze, in standard row-column order, separated by
     * whitespace.  Use this to initialize the state of each cell.
     * @param s the string of all cell data
     */
    void readMaze(String s);


    /**
     * Clear the visited and path fields of all the cells in the maze,
     * resetting them to false.
     */
    void clearSearch();


    /**
     * Perform a depth-first-search on the maze to find a path from
     * the start (top-left cell) to the finish (bottom-right cell),
     * using a Stack (recursion is forbidden).
     * @param s the stack to use in finding a solution
     */
    void solveDFS(Stack<Cell> s);


    /**
     * Perform a breadth-first-search on the maze to find a path from
     * the start (top-left cell) to the finish (bottom-right cell),
     * using a Queue (recursion is forbidden).
     * @param q the queue to use in finding a solution
     */
    void solveBFS(Queue<int[]> q);


    /**
     * This method must create and return one (long) string that
     * contains the row and column dimensions of the maze, then a
     * newline, followed by the string representation of each cell,
     * one row at a time, with each cell separated from the next with
     * a space and each row separated from the next by a newline
     * ('\n').
     * @return the string representation
     */
    String toString();


}