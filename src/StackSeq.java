

/**
 * The implementation of a stack using SequenceDLL.
 * @author TKeady
 *
 * @param <T> The type of objects inside the stack 
 */
public class StackSeq<T> implements Stack<T> {
    
    /**
     * The internal class holding the data in a SequenceDLL.
     * THE TAIL IS THE TOP OF THE STACK
     */
    private Sequence226<T> stack;
    
    
   /**
    * 
    */
    // do you need variables here that point to the head/tail?
    
    public StackSeq() {
        this.stack = new Sequence226<T>();
    }
    
    
    
    /**
     * Put a new item on the top of the stack.
     * @param item the object to push
     */
    public void push(T item) {
        this.stack.addAt(item, this.stack.size());
    }


    /** 
     * Return (not remove) the item at the top of the stack.
     * @return the item, or null if the stack is empty
     */
    public T top() {
        
        return this.stack.getAt(this.stack.size() - 1);
    }


    /** 
     * Remove the item at the top of the stack.
     * @return the item removed, or null if the stack is empty
     */
    public T pop() {
        return this.stack.removeAt(this.stack.size() - 1);
    }


    /**
     * Remove all existing elements from the stack.
     */
    public void clear() {
        this.stack.clear();
    }


    /**
     * Report whether the stack is empty or not.
     * @return true if the stack is empty; false otherwise
     */
    public boolean isEmpty() {
        return this.stack.isEmpty();
    }


    /**
     * Return the number of items present in the stack.
     * @return the number of items
     */
    public int size() {
        return this.stack.size();
    }


    /**
     * Return a representation of the items in the stack, in order
     * from bottom to top, separated by ' - ' with each enclosed in
     * '[]', including 'BOT' and 'TOP' labels.  For example, if the
     * stack contains three items with item3 at the top, the returned
     * value will be "BOT - [item1] - [item2] - [item3] - TOP"
     * @return the formatted string
     */
    public String toString() {
        String toReturn = "BOT ";
        
        /*Iterator<T> iter = this.stack.iterator();
        
        while (iter.hasNext()) {
            toReturn = toReturn + "- " + iter.next().   
            // How get data T from object? Don't know name in Sequence226
        }*/
        
        int end = this.stack.size();
        for (int i = 0; i < end; ++i) {
            toReturn = toReturn + "- [" + this.stack.getAt(i).toString() + "] ";
            
        }
        
        return toReturn + "- TOP";
        
        
    }

}
