// Thomas Keady tkeady1 600.226(01) P2A

/**
 * Every cell has 4 to begin with, then we knock them down.
 * To connect two adjacent cells, knock down two walls.  
 * @author TKeady
 *
 */
public class Wall {
    
    /**
     * Cell which this wall is a part of.
     */
    private int cell;
    
    /**
     * Side of cell that it is on.
     */
    private String side;
    
    
    /**
     * Contsructor for a wall, gives it a cell and side.  
     * This is sufficient to make all walls unique.  
     * @param c The number of the cell.
     * @param s The side of the cell the wall is on
     */
    public Wall(int c, String s) {
        this.cell = c;
        this.side = s;
    }
    
    
    /**
     * For debugging purposes only (right?).
     */
    @Override
    public String toString() {
        return this.cell + " " + this.side;
    }
    
    
    /**
     * Returns whether two walls are equal.
     * Currently only checks their internal variables.
     * Could possibly make it so that it checks whether they
     * both need to be removed at once, i.e. if they are the
     * "same" wall in the maze, but not doing that i dont think.  
     */
    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Wall)) {     // Check that we are dealing with a Wall
            return false;
        }
        
        Wall rhs = (Wall) o;            // For good measure
        
        // Check equality
        return (this.cell == rhs.cell) && (this.side.equals(rhs.side));
    }
    
    /**
     * Because eclipse and the internet told me to.  
     */
    @Override
    public int hashCode() {
        return this.side.hashCode();
    }
    
    
    /**
     * Get the cell number of this wall.
     * @return The cell number
     */
    public int getCell() {
        return this.cell;
    }
    
    /**
     * Get the direction of this wall.
     * @return The side of the wall
     */
    public String getSide() {
        return this.side;
    }
    
    
}
