/** 
 * Generic stack interface.
 *
 * 600.226 Data Structures, Fall 2015, Project 2
 *
 * @param <T> the underlying type stored in the list
 */

public interface Stack<T> {

    /**
     * Put a new item on the top of the stack.
     * @param item the object to push
     */
    void push(T item);


    /** 
     * Return (not remove) the item at the top of the stack.
     * @return the item, or null if the stack is empty
     */
    T top();


    /** 
     * Remove the item at the top of the stack.
     * @return the item removed, or null if the stack is empty
     */
    T pop();


    /**
     * Remove all existing elements from the stack.
     */
    void clear();


    /**
     * Report whether the stack is empty or not.
     * @return true if the stack is empty; false otherwise
     */
    boolean isEmpty();


    /**
     * Return the number of items present in the stack.
     * @return the number of items
     */
    int size();


    /**
     * Return a representation of the items in the stack, in order
     * from bottom to top, separated by ' - ' with each enclosed in
     * '[]', including 'BOT' and 'TOP' labels.  For example, if the
     * stack contains three items with item3 at the top, the returned
     * value will be "BOT - [item1] - [item2] - [item3] - TOP"
     * @return the formatted string
     */
    String toString();

}