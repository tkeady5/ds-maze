// Thomas Keady tkeady1 EN.600.226(01) P2B

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

import org.junit.Before;
import org.junit.Test;

import java.util.Iterator;
import java.util.ConcurrentModificationException;
import java.util.NoSuchElementException;

/**
 * @author TKeady
 *
 */
public class SequenceTest {
    
    Sequence<Integer> s1;
    Sequence<String> s2;
    
    @Before
    public void setup() {
        s1 = new SequenceDLL<Integer>();
        s2 = new SequenceDLL<String>();
        
    }
    
    @Test
    public void addTest() {
        assertTrue(s1.isEmpty());
        s1.addAt(1, 0);
        assertTrue(1 == s1.getAt(0));
        assertTrue(!s1.isEmpty());
        assertTrue(1 == s1.size());
        assertTrue(s1.contains(1));
        
    }
    
    @Test
    public void addTest1() {
        
        s1.addAt(1, 0);
        assertTrue(!s1.isEmpty());
        assertTrue(1 == s1.getAt(0));
        //assertEquals("[1]", s1.toString());

        
        s1.addAt(2, 1);
        //assertEquals("[1, 2]", s1.toString());
        assertTrue(1 == s1.getAt(0));
        assertTrue(!s1.isEmpty());
        assertTrue(s1.size() == 2);
        assertTrue(2 == s1.getAt(1));

        
        s1.addAt(3, 5);
        //assertEquals("[1, 2, 3]", s1.toString());
        assertTrue(1 == s1.getAt(0));
        assertTrue(2 == s1.getAt(1));
        assertTrue(3 == s1.getAt(2));
        assertTrue(s1.size() == 3);
        
        s1.addAt(0, -10);
        //assertEquals("[0, 1, 2, 3]", s1.toString());
        assertTrue(0 == s1.getAt(0));
        assertTrue(1 == s1.getAt(1));
        assertTrue(2 == s1.getAt(2));
        assertTrue(3 == s1.getAt(3));
        assertTrue(s1.size() == 4);
        
    }
    
    @Test
    public void addTest2() {
        s2.addAt("S", 100);
        assertTrue("S" == s2.getAt(0));
        
        s2.addAt("A", 1);
        assertTrue("S" == s2.getAt(0));
        assertTrue("A" == s2.getAt(1));
        
        s2.addAt("U", 0);
        assertTrue("U" == s2.getAt(0));
        assertTrue("S" == s2.getAt(1));
        assertTrue("A" == s2.getAt(2));
        
    }
    
    @Test
    public void getAtTest() {
        
        assertTrue(null == s1.getAt(1));
        assertTrue(null == s1.getAt(0));
        
        s1.addAt(1, 0);
        assertTrue(1 == s1.getAt(0));
        //assertEquals("[1]", s1.toString());
        assertTrue(null == s1.getAt(1));
        
        s1.addAt(2, 1);
        //assertEquals("[1, 2]", s1.toString());
        assertTrue(1 == s1.getAt(0));
        assertTrue(2 == s1.getAt(1));
        assertTrue(null == s1.getAt(2));
        
        s1.addAt(3, 5);
        //assertEquals("[1, 2, 3]", s1.toString());
        assertTrue(1 == s1.getAt(0));
        assertTrue(2 == s1.getAt(1));
        assertTrue(3 == s1.getAt(2));
        
        s1.addAt(0, -10);
        //assertEquals("[0, 1, 2, 3]", s1.toString());
        assertTrue(0 == s1.getAt(0));
        assertTrue(1 == s1.getAt(1));
        assertTrue(2 == s1.getAt(2));
        assertTrue(3 == s1.getAt(3));
        assertTrue(null == s1.getAt(5));
        assertTrue(null == s1.getAt(10));
        assertTrue(null == s1.getAt(-10));
    }
    
    public void getAtTest1() {
        s1.addAt(7, 0);
        s1.addAt(4, 1);
        s1.addAt(1776, 2);
        
        assertTrue(7 == s1.getAt(0));
        assertTrue(4 == s1.getAt(1));
        assertTrue(1776 == s1.getAt(2));
    }
    
    
    @Test 
    public void removeAtTest() {
        assertTrue(null == s1.removeAt(-10));
        assertTrue(null == s1.removeAt(0));
        assertTrue(null == s1.removeAt(10));
        
        assertTrue(null == s2.removeAt(-10));
        assertTrue(null == s2.removeAt(0));
        assertTrue(null == s2.removeAt(10));
    }
    
    @Test
    public void removeAtTest1() {
        s1.addAt(1, 0);
        s1.addAt(2, 1);
        s1.addAt(3, 2);
        s1.addAt(4, 3);
        s1.addAt(5, 4);
        
        assertTrue(null == s1.removeAt(-10));
        assertTrue(null == s1.removeAt(10));
        assertTrue(null == s1.removeAt(5));
        assertTrue(5 == s1.removeAt(4));
        
        assertTrue(1 == s1.removeAt(0));
        assertTrue(null == s1.removeAt(4));
        
        assertEquals("[2, 3, 4]", s1.toString());
        
        assertTrue(3 == s1.removeAt(1));
        
        assertEquals("[2, 4]", s1.toString());
        
    }
    
    @Test 
    public void removeAtTest2() {
        s2.addAt("z", 0);
        s2.addAt("q", 1);
        s2.addAt("J", 2);
        s2.addAt("o", 3);
        s2.addAt("h", 4);
        s2.addAt("n", 5);
        s2.addAt("C", 6);
        s2.addAt("e", 7);
        s2.addAt("n", 8);
        s2.addAt("a", 9);
        s2.addAt("s", 10);
        
        assertTrue("e" == s2.removeAt(7));
        assertTrue("z" == s2.removeAt(0));
        assertTrue("n" == s2.removeAt(6));
        assertTrue("q" == s2.removeAt(0));
        assertTrue("a" == s2.removeAt(5));
        assertTrue("C" == s2.removeAt(4));
        
        assertEquals("[J, o, h, n, s]", s2.toString());
        
        assertTrue("s" == s2.removeAt(4));
        
        assertFalse("[J, o, h, n, s]" == s2.toString()); // Embrace
        
    }
    
    @Test 
    public void setAtTest() {
        assertTrue(null == s1.setAt(-1, 5));
        assertTrue(null == s1.setAt(0, 5));
        assertTrue(null == s1.setAt(1, 5));
        
        s1.addAt(1, 0);
        s1.addAt(2, 1);
        s1.addAt(3, 2);
        
        assertTrue(1 == s1.setAt(4, 0));
        assertTrue(2 == s1.setAt(5, 1));
        assertTrue(3 == s1.setAt(6, 2));
        assertTrue(null == s1.setAt(3, -1));
        assertTrue(null == s1.setAt(7, 3));
                
    }
    
    
    @Test
    public void setAtTest1() {
        s2.addAt("A", 0);
        s2.addAt("B", 1);
        s2.addAt("C", 2);
        
        assertTrue("C" == s2.setAt("U", 2));
        assertTrue("B" == s2.setAt("H", 1));
        assertTrue("A" == s2.setAt("J", 0));
        
        
        assertTrue(null == s2.getAt(-1));
        assertTrue("J" == s2.getAt(0));
        assertTrue("H" == s2.getAt(1));
        assertTrue("U" == s2.getAt(2));
        assertTrue(null == s2.getAt(3));
        
    }
    
    
    @Test
    public void clearTest() {
        assertTrue(s1.size() == 0);
        s1.clear();
        assertTrue(s1.size() == 0);
        
        assertTrue(s2.size() == 0);
        s2.clear();
        assertTrue(s2.size() == 0);
        
    }
    
    
    @Test
    public void clearTest1() {
        
        assertTrue(s1.size() == 0);
        s1.addAt(6, 0);
        s1.addAt(3, 1);
        s1.addAt(1989, 2);
        s1.clear();
        assertTrue(s1.size() == 0);
        
        
    }
    
    @Test
    public void clearTest2() {
        
        assertTrue(s2.size() == 0);
        s2.addAt("H", 0);
        s2.addAt("A", 1);
        s2.addAt("T", 2);
        s2.addAt("E", 3);
        s2.clear();
        assertTrue(s2.size() == 0);
        
        
    }
    
    @Test
    public void sizeTest() {
        assertTrue(s1.size() == 0);
        assertTrue(s2.size() == 0);
    }
    
    @Test
    public void sizeTest1() {
        s2.addAt("A", 0);
        assertTrue(s2.size() == 1);
        s2.addAt("B", 1);
        assertTrue(s2.size() == 2);
        s2.addAt("C", 2);
        assertTrue(s2.size() == 3);
        s2.addAt("D", 3);
        assertTrue(s2.size() == 4);
        s2.addAt("E", 4);
        assertTrue(s2.size() == 5);
        s2.addAt("F", 5);
        assertTrue(s2.size() == 6);
        s2.addAt("G", 6);
        assertTrue(s2.size() == 7);
        s2.addAt("H", 7);
        assertTrue(s2.size() == 8);
        s2.addAt("I", 8);
        assertTrue(s2.size() == 9);
    }

    @Test
    public void sizeTest2() {
        assertTrue(s1.size() == 0);
        for (int i = 0; i < 100; ++i) {
            s1.addAt(i, 1);
            
        }
        
        for (int i = 0; i < 100; ++i) {
            s1.addAt(-1, -1);
        }
        
        for (int i = 0; i < 100; ++i) {
            s1.addAt(1, 200);
        }
        
        
        assertTrue(s1.size() == 300);
        
    }
    
    
    @Test
    public void isEmptyTest() {
        assertTrue(s1.isEmpty() == true);
        assertTrue(s2.isEmpty() == true);
    }
    
    
    @Test
    public void isEmptyTest1() {
        
        s1.addAt(1, -1);
        assertTrue(s1.isEmpty() == false);
        
        s2.addAt("One", -1);
        assertTrue(s2.isEmpty() == false);
    }
    
    @Test
    public void isEmptyTest2() {
        
        s1.addAt(1, 0);
        assertTrue(s1.isEmpty() == false);
        
        s2.addAt("One", 0);
        assertTrue(s2.isEmpty() == false);
    }
    
    
    @Test
    public void isEmptyTest3() {
        
        s1.addAt(1, 10);
        assertTrue(s1.isEmpty() == false);
        
        s2.addAt("One", 1000);
        assertTrue(s2.isEmpty() == false);
    }
    
    
    
    @Test
    public void containsTest() {
        assertTrue(!s1.contains(0));
        assertTrue(!s2.contains("*"));
    }
    
    
    @Test
    public void containsTest1() {
        s1.addAt(1, 0);
        s1.addAt(2, 1);
        s1.addAt(3, 2);
        
        assertTrue(!s1.contains(0));
        assertTrue(s1.contains(1));
        assertTrue(s1.contains(2));
        assertTrue(s1.contains(3));
        assertTrue(!s1.contains(4));
        
    }
    
    @Test
    public void containsTest2() {
        s2.addAt("U", 0);
        s2.addAt("S", 1);
        s2.addAt("A", 2);
        
        assertTrue(!s2.contains("Z"));
        assertTrue(s2.contains("U"));
        assertTrue(s2.contains("S"));
        assertTrue(s2.contains("A"));
        assertTrue(!s2.contains("T"));
        
    }
    
    public void positionTest() {
        assertTrue(-1 == s1.position(0));
        assertTrue(-1 == s1.position(1));
        assertTrue(-1 == s1.position(2));
        assertTrue(-1 == s1.position(-1));
        assertTrue(-1 == s1.position(10));
        
        assertTrue(-1 == s2.position("A"));
        assertTrue(-1 == s2.position("B"));
        assertTrue(-1 == s2.position("C"));
        assertTrue(-1 == s2.position("Z"));
        assertTrue(-1 == s2.position("*"));
    }
    
    @Test
    public void positionTest1() {
        s1.addAt(7, 0);
        s1.addAt(4, 1);
        s1.addAt(1776, 2);
        
        assertTrue(0 == s1.position(7));
        assertTrue(1 == s1.position(4));
        assertTrue(2 == s1.position(1776));
        
    }
    
    
    @Test
    public void positionTest2() {
        s2.addAt("Why", -1);
        s2.addAt("are", 1);
        s2.addAt("the functions", 2);
        s2.addAt("named ___At()", 3);
        s2.addAt("if the first", 4);
        s2.addAt("parameter", 5);
        s2.addAt("is the item", 6);
        s2.addAt("and the second", 7);
        s2.addAt("is the position. You", 8);
        s2.addAt("are", 9);
        s2.addAt("adding", 15);
        s2.addAt("AT the position, not", 11);
        s2.addAt("AT the item!", 100);
        
        assertTrue(0 == s2.position("Why"));
        assertTrue(1 == s2.position("are"));
        assertTrue(1 == s2.position("are"));
        assertTrue(12 == s2.position("AT the item!"));
        assertTrue(0 == s2.position("Why"));
        assertTrue(5 == s2.position("parameter"));
        
    }

    
    
    @Test
    public void toStringTest() {
        s2.addAt("Why", -1);
        s2.addAt("are", 1);
        s2.addAt("the functions", 2);
        s2.addAt("named ___At()", 3);
        s2.addAt("if the first", 4);
        s2.addAt("parameter", 5);
        s2.addAt("is the item", 6);
        s2.addAt("and the second", 7);
        s2.addAt("is the position. You", 8);
        s2.addAt("are", 9);
        s2.addAt("adding", 15);
        s2.addAt("AT the position, not", 11);
        s2.addAt("AT the item!", 100);
        
        assertEquals("[Why, are, the functions, named ___At(), "
                + "if the first, parameter, is the item, and "
                + "the second, is the position. You, are, "
                + "adding, AT the position, not, AT the item!]", 
                s2.toString());
        
    }
    
    @Test 
    public void toStringTest1() {
        assertEquals("[]", s1.toString());
        assertEquals("[]", s2.toString());
    }
    
    
    @Test 
    public void toStringTest2() {
        s1.addAt(3, 0);
        s1.addAt(1, 1);
        s1.addAt(4, 2);
        s1.addAt(1, 3);
        s1.addAt(5, 4);
        s1.addAt(9, 5);
        s1.addAt(2, 6);
        s1.addAt(6, 7);
        s1.addAt(5, 8);
        
        assertEquals("[3, 1, 4, 1, 5, 9, 2, 6, 5]", s1.toString());
        
        
    }
    
    @Test
    public void toSTringTest3() {
        
        s1.addAt(1, 0);
        assertEquals("[1]", s1.toString());
        
        s1.addAt(2, 1);
        assertEquals("[1, 2]", s1.toString());
        
        s1.addAt(3, 5);
        assertEquals("[1, 2, 3]", s1.toString());
        
        s1.addAt(0, -10);
        assertEquals("[0, 1, 2, 3]", s1.toString());
        
    }
    
    
    @Test (expected = IllegalStateException.class)
    public void iteratorTest() {
        Iterator<Integer> iter = this.s1.iterator();
        
        iter.remove();
        
    }
    
    @Test (expected = ConcurrentModificationException.class)
    public void iteratorTest1() {
        s1.addAt(1, 0);
        s1.addAt(2, 1);
        
        Iterator<Integer> iter = this.s1.iterator();
        
        assertTrue(iter.hasNext());
        assertTrue(iter.next() == 1);
        
        s1.addAt(3, 2);
        
        iter.hasNext();
        
    }
    
    
    @Test 
    public void iteratorTest2() {
        s1.addAt(1, 0);
        s1.addAt(2, 1);
        s1.addAt(3, 2);
        s1.addAt(4, 3);
        
        Iterator<Integer> iter = this.s1.iterator();
        
        assertTrue(iter.hasNext());
        assertTrue(iter.next() == 1);
        assertTrue(iter.hasNext());
        assertTrue(iter.next() == 2);
        assertTrue(iter.hasNext());
        assertTrue(iter.next() == 3);
        assertTrue(iter.hasNext());
        assertTrue(iter.next() == 4);
        assertTrue(!iter.hasNext());
        
    }
    
    @Test
    public void iteratorTest3() {
        s2.addAt("z", 0);
        s2.addAt("q", 1);
        s2.addAt("J", 2);
        s2.addAt("o", 3);
        s2.addAt("h", 4);
        s2.addAt("n", 5);
        s2.addAt("C", 6);
        s2.addAt("e", 7);
        s2.addAt("n", 8);
        s2.addAt("a", 9);
        s2.addAt("s", 10);
        
        Iterator<String> iter = this.s2.iterator();
        
        assertTrue(iter.hasNext());
        assertTrue(iter.next() == "z");
        iter.remove();
        assertTrue(iter.hasNext());
        assertTrue(iter.next() == "q");
        iter.remove();
        assertTrue(iter.hasNext());
        assertTrue(iter.next() == "J");
        assertTrue(iter.hasNext());
        assertTrue(iter.next() == "o");
        assertTrue(iter.hasNext());
        assertTrue(iter.next() == "h");
        assertTrue(iter.hasNext());
        assertTrue(iter.next() == "n");
        assertTrue(iter.hasNext());
        assertTrue(iter.next() == "C");
        iter.remove();
        assertTrue(iter.hasNext());
        assertTrue(iter.next() == "e");
        iter.remove();
        assertTrue(iter.hasNext());
        assertTrue(iter.next() == "n");
        assertTrue(iter.hasNext());
        iter.remove();
        assertTrue(iter.next() == "a");
        iter.remove();
        assertTrue(iter.hasNext());
        assertTrue(iter.next() == "s");
        assertTrue(!iter.hasNext());
        
        assertEquals("[J, o, h, n, s]", s2.toString());
        
        iter.remove();
        
        assertEquals("[J, o, h, n]", s2.toString());
        // No embrace :'(
    }
    
    @Test (expected = NoSuchElementException.class)
    public void iteratorTest4() {
        s1.addAt(99, 0);
        s1.addAt(98, 1);
        s1.addAt(97, 2);
        
        Iterator<Integer> iter = s1.iterator();
        
        assertTrue(iter.next() == 99);
        assertTrue(iter.next() == 98);
        assertTrue(iter.next() == 97);
        iter.next();
        
    }
    
    
}


