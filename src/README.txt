Thomas Keady tkeady1 600.226(01) P2A

As far as I know, my code works as intended.  It can properly create mazes of any dimensions 
that only have one solution.  The majority of the code in UnionFind.java has been copied from
http://algoviz.org/OpenDSA/Books/CS226JHUF15/html/UnionFind.html and cited.  

I zipped all files including those that I did not modify (BaG.java, Cell.java, Maze.java,
MazeDriver.java, MazeRenderer.java) along with the tiles.  Running my program in Eclipse with 
this setup everything works fine.  
