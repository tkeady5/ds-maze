/**
 * The implementation of a queue using SequenceDLL.
 * @author TKeady
 *
 * @param <T> The type of objects inside the stack 
 */
public class QueueSeq<T> implements Queue<T> {
    
    /**
     * The internal class holding the data in a SequenceDLL.
     * THE TAIL IS THE END/BACK OF THE QUEUE WHERE THINGS GET ENQUEUED
     */
    private Sequence226<T> queue;
    
    
   /**
    * 
    */
    // do you need variables here that point to the head/tail?
    
    public QueueSeq() {
        this.queue = new Sequence226<T>();
    }
    
    
    /**
     * Add a new item to the end (back) of the queue (where things
     * get added to).
     * @param item the object to add
     */
    public void enqueue(T item) {
        this.queue.addAt(item, 0);
    }


    /** 
     * Return (not remove) the item at the front of the queue.
     * @return the item, or null if the queue is empty
     */
    public T peek() {
        return this.queue.getAt(this.queue.size() - 1);
    }


    /** 
     * Remove the item at the front of the queue.
     * @return the item removed, or null if the queue is empty
     */
    public T dequeue() {
        return this.queue.removeAt(this.queue.size() - 1);
    }


    /**
     * Remove all existing elements from the queue.
     */
    public void clear() {
        this.queue.clear();
    }


    /**
     * Report whether the queue is empty or not.
     * @return true if the queue is empty; false otherwise
     */
    public boolean isEmpty() {
        return this.queue.isEmpty();
    }


    /**
     * Return the number of items present in the queue.
     * @return the number of items
     */
    public int size() {
        return this.queue.size();
    }


    /**
     * Return a representation of the items in the queue, in order
     * from front to back, separated by ' __ ', including 'FRONT' and
     * 'BACK' labels.  For example, if the queue contains three items,
     * with item1 at the front, the returned value will be
     * "FRONT __ item1 __ item2 __ item3 __ BACK"
     * @return the formatted string
     */
    public String toString() {
        
        String toReturn = "FRONT __ ";
        
        int end = this.queue.size();
        for (int i = end - 1; i >= 0; --i) {
            toReturn = toReturn + this.queue.getAt(i)  + " __ ";
        }
        
        return toReturn + "BACK";
        
        
    }

}
